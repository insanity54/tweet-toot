# Tweet-Toot
Tweet-Toot is a small Python3 project to convert a tweet to a toot.

It's basically a Twitter relay for Mastodon :)

Just clone it, configure it, schedule it, and it will toot new tweets at a Mastodon of your choice.

---

## How do I install this?
Getting Tweet-Toot working is pretty easy. Before you can install it, you're going to need to do the following:

- Pick a Mastodon instance of your choice. You'll need this instance's URL.
- Create an app on this Mastodon instance and generate an access token.
- Get the Twitter URL of the account you want to watch.

Once you have the above, just follow these steps:

1. Clone this repository.
2. Install the Python3 libraries by running these commands:

 - `python3 -m venv venv`
 - `source venv/bin/activate`
 - `pip3 install -r tweet-toot/requirements.txt`

3. In `config.json`, update the following:

- `TT_SOURCE_TWITTER_URL`: The Twitter account URL. Separate multiple value with `,`.
- `TT_HOST_INSTANCE`: The Mastodon instance URL. Separate multiple value with `,`.
- `TT_APP_SECURE_TOKEN`: The Mastodon app access token. Separate multiple value with `,`.
- `TT_CACHE_PATH`: Cache path. This is where we keep the last tweet, so keep this fixed.
- `TT_MODE`: Mode for Tweet-Toot when multiple Twitter or Mastodon URLs are provided. See details below.
- `TT_TOOT_LIMIT`: Limits the number of toots per Toot Tweet invocation. Set to -1 to disable.

For example:

- `TT_SOURCE_TWITTER_URL` = https://mobile.twitter.com/internetofshit
- `TT_HOST_INSTANCE` = https://botsin.space
- `TT_APP_SECURE_TOKEN` = XXXXX-XXXXX-XXXXX-XXXXX-XXXXX'
- `TT_CACHE_PATH` = `/tmp`
- `TT_TOOT_LIMIT` = `1`

---

## How do I run it?
Once it's all setup, execute the app by running:

```bash
source venv/bin/activate
cd tweet-toot
python run.py
```

If all goes well, you'll see something like this:
```bash
2020-10-12 05:20:17,243 - __main__ - INFO - __main__ => Mode: one-to-one
2020-10-12 05:20:17,244 - tweettoot - INFO - relay() => Init relay from https://mobile.twitter.com/ebay_sbtp to https://wxw.moe. State file /tmp/tt_d573beb3b0cd36cdc8f2a65fd45e1bbf28b851cc
2020-10-12 05:20:18,461 - tweettoot - INFO - get_tweets() => Fetched 20 tweets for https://mobile.twitter.com/ebay_sbtp.
2020-10-12 05:20:28,832 - tweettoot - INFO - relay() => Tooting 1315381657495429121 to https://wxw.moe
2020-10-12 05:20:35,733 - tweettoot - INFO - _upload_media() => uploaded as 105021782832396313
2020-10-12 05:20:36,961 - tweettoot - INFO - toot_the_tweet() => OK.
2020-10-12 05:20:36,963 - tweettoot - INFO - relay() => Limiting to 1 toot(s).
```

### Running it in background on windows
This can easily be done with [nircmd](https://www.nirsoft.net/utils/nircmd.html).
1. Create a .bat file that writes to a log file:

```
@ECHO OFF
cd "C:\yourpath\tweet-toot\tweet-toot\"
"C:\yourpath\python.exe" "run.py" > output.log 2>&1
```
2. Create a task in the task scheduler with the following action:
program: ```"C:\yourpath\nircmd.exe"```
arguments: ```exec hide "C:\yourpath\runWithLogFile.bat"```

---

## How does it work?
The tutorial for this code can be found here: [Tweet-Toot: Building a bot for Mastodon using Python](https://notes.ayushsharma.in/2018/09/tweet-toot-building-a-bot-for-mastodon-using-python).

If you like the tutorial, don't forget to spread the word on Mastodon :)

---

## How do I build the Docker image and run it?
I've added a `Dockerfile` with this repo so you can get up and running with Docker quickly.

### To build the Docker image locally:

1. Clone this repo.

2. In the main directory, run:

   ```
   docker build -t tweet-toot:latest -f Dockerfile tweet-toot
   ```

3. Export your Mastodon token in your environment:

   ```
   export TT_APP_SECURE_TOKEN="<token>"
   ```

   We'll pass this to the container later. No need to hard-code the `config.json`.

4. Execute the container:

   ```
   docker run --rm -e TT_APP_SECURE_TOKEN="$TT_APP_SECURE_TOKEN" -v /tmp:/tmp tweet-toot:latest
   ```

   We need `TT_CACHE_PATH` same across `docker run`s, so we're mounting a local directory into the container's `/tmp`. Customise as you see fit.

   To override more config paramters, just pass more `-e`s to Docker.

---

## What's up with `TT_MODE`?
As Tweet-Toot has grown, there have been requests over the years to allow relaying many Twitter accounts to the same Mastodon instance, or relay one Twitter account to many Mastodons, etc. As you can imagine, there are 4 possible scenarios:

1. One Twitter account posts to a single Mastodon (default behaviour).
2. One Twitter account posts to many Mastodons.
3. Many Twitter accounts post to a single Mastodon.
4. Many Twitter accounts post to many Mastodons.

The way this works is this. The `TT_SOURCE_TWITTER_URL`, `TT_HOST_INSTANCE`, and `TT_APP_SECURE_TOKEN` values are split by `,`, and the rest of the processing follows the value of `TT_MODE`.

- `TT_MODE: one-to-one`: In this mode, the first Twitter URL is picked and it is relayed to the first Mastodon URL/Token combination. This repeats until we run out of Twitter URLs or Mastodon URLs. The number of Twitter accounts must be equal to the number of Mastodon URLs/tokens to avoid wierdness.

- `TT_MODE: one-to-many`: In this mode, the first Twitter URL is picked and relayed to all Mastodon instances. Then the next Twitter URL is picked.

- `TT_MODE: many-to-one`: In this mode, every Twitter account is relayed to a single Mastodon instance.

- `TT_MODE: many-to-many`: In this mode, every Twitter account is relayed to all Mastodon instances.

Remember, the number of values in `TT_HOST_INSTANCE` and `TT_APP_SECURE_TOKEN` should always be equal so that we can pick the token for every instance.

This `,` structure is put in place so that the `config.json` values can still be specified as `env` variables.

---

## Things to remember
- When running for the first time, the script will only toot the latest tweet. After that it will toot all new tweets.
- The script only checks the 20 latest tweets (the value 20 is set by the twitter html response). I recommend timing your cron jobs according to the post frequency that you need.
- Mastodon instance admins are people too. Don't toot-blast your instance and make life harder for them.
- When configuring your bot, ensure you clearly display an account where you can be reached in case of issues.

Have fun :)

---

For questions or contributions, please create an issue.
